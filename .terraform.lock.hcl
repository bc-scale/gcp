# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/gitlabhq/gitlab" {
  version = "3.8.0"
  hashes = [
    "h1:AAg5ZjtEeLWo/X4vpLe/UKB3R+Cn0kDyIhTkcuv1vpE=",
    "zh:02d977e6803ff336054194e567fcd11d095846346a604017afef98ada0353bc1",
    "zh:07294f6061db86c1ed44e5eae01576d76c585128e334761feb01fca5b866d53f",
    "zh:1cfa4cbbdb9a8a235bb82e7f3968982993750fc5a95bd948b9dfd6de772bff0a",
    "zh:3f9172206c00de224e3e234503681c29f918e2b83dbf5b11c0e791694600bec6",
    "zh:71174040a680be48f65b63fb5571bdd76cd17de830c88b81b6e684b56d77616c",
    "zh:81b1c86b8b59c4f3faed59d461555ce53d86b12f9fc91a2afaee89ca3992f154",
    "zh:a09d2012b4392c01a310713c170db1d7e211190b0e2a27e43ec2f7ca59b6d6c9",
    "zh:a97ed255d3d161313370fc34e83018216dcf198e7a5b285abe31b26bf8b048f3",
    "zh:abfad232ec29ecea620e087d20f1586d0b5b4fc02a6369a6b0c65e80b8de3566",
    "zh:c8814ae34933a082bf68db958ed6ef708321093ce9901a11e2efceaa5b3ed031",
    "zh:e5a52eaada2797fbec6aca19bc7a74cdb40dc1625df999f6a93c34690ea2bc80",
    "zh:f4e1787f9fcab92824dad41486eaa6895a789c2d25f3d3f3126d134bd5edfe90",
    "zh:f90b196e6ff444fe721603765a5be5778bc92d45458009bc77ee6d1b6c32bf67",
  ]
}

provider "registry.terraform.io/hashicorp/google" {
  version = "4.1.0"
  hashes = [
    "h1:55MGqDSJVO4twY7pcRk0IbEJpzc2OIYg4Eivwpkw2lc=",
    "zh:3477415ef4ef02d7c065a08fb24b7011ddd77f76b2a011db25e0b73c767acaae",
    "zh:43be16d0bbb56d29c090c1abe49e877e4f3ca66bb372a661a2cd0a377cd61e8f",
    "zh:6bb202a40055ed302f9dd05e2f3f87aa13080e580a9ac7b0241d634b78b68a2a",
    "zh:7179303f628c5fc58129013ef4a4e754e225e266e68af4a7167a06a64d9d97db",
    "zh:837cdcf1bafd4c936f15a4a22353efeec96ca23727f8d8ff22e9d53414d87084",
    "zh:a853f1a1e9f5ac261fd9a02d869ce7da16917fdd16464dabf2200e2ccd04fba9",
    "zh:aba631b9db47cc34857c25459475f17d939a46022f886e396a8254588f720c58",
    "zh:ac461358984ba480b4fef1d2be3538d9714aa8814a6f420e1e954227054a14a4",
    "zh:ade64689485fc28b6aa0c36861c6c9c66b5d7a8e7282739ecf97c7791b973312",
    "zh:c3fbbfe7f6e870e19bb552a2ad84ea38c9ddf3e25e0b7e996ecb2fc7ce336b03",
    "zh:d1419f4fe9c07f136ee9015a299cd6acf25a13bea0afa0f69727a576058603d3",
  ]
}

provider "registry.terraform.io/hashicorp/kubernetes" {
  version = "2.6.1"
  hashes = [
    "h1:ozWeiqW1iLYIhT1VGkkA9nRO6NbwUUPuksE9v7Vxth4=",
    "zh:081fbaf9441ebb278753dcf05f318fa7d445e9599a600d7c525e9a18b871d4c8",
    "zh:143bfbe871c628981d756ead47486e807fce876232d05607e0b8852ebee4eed8",
    "zh:34f413a644eb952e3f041d67ef19200f4c286d374eae87b60fafdd8bf6bb5654",
    "zh:370562be70233be730e1876d565710c3ef477e047f209cb3dff8a4a3217a6461",
    "zh:443021df6d56e59e4d8dda8e57b506affff32b8a22de09661d21b98bc781fefb",
    "zh:51a9501360b58adf9ee6e09fb81f555042ebc909ab36e06ccfc5e701e91f9923",
    "zh:7d41d48b8291b98e0a4b7a1f79a9d1fe140a2e0d8df422c5b48cbae4c3fa615a",
    "zh:881b3e44814d7d49a5820e2e4b13ee3d000b5baf7957df774a909f17472ece8a",
    "zh:b860ff68a944de63fbe0a624c41f2e373711a2da4298c0f0cb151e00fb32a6b3",
    "zh:c4ab48ea6e0f8d4a6db1abab1877addb2b21ecd126e505c74b8c85804bd92cbe",
    "zh:e96589575dfd31eab48fcc85466dd49895925473c60c802b346cdb4037953350",
  ]
}
