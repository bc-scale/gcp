export GITLAB_ACCESS_TOKEN=<YOUR-ACCESS-TOKEN>
terraform init \
    -backend-config="address=https://gitlab.com/api/v4/projects/31449700/terraform/state/gcp" \
    -backend-config="lock_address=https://gitlab.com/api/v4/projects/31449700/terraform/state/gcp/lock" \
    -backend-config="unlock_address=https://gitlab.com/api/v4/projects/31449700/terraform/state/gcp/lock" \
    -backend-config="username=py-paulo" \
    -backend-config="password=$GITLAB_ACCESS_TOKEN" \
    -backend-config="lock_method=POST" \
    -backend-config="unlock_method=DELETE" \
    -backend-config="retry_wait_min=5"
